﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RodneyRibeiro.Models;
using System.Web.Hosting;
using System.Web.Security;

namespace RodneyRibeiro.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [UserFiltro]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User l)
        {
            try
            {
                using (UserModel model = new UserModel())
                {
                    User user = model.Read(l.Email, l.Senha);
                    if (user == null)
                    {
                        ViewBag.Erro = false;
                        return View();
                    }
                    else
                    {
                        Session["usuario"] = user;
                        return RedirectToAction("Index");
                    }
                }
            }
            catch
            {
                ViewBag.Erro = false;
                return View();
            }
        }

        public ActionResult ForgetPass()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User c)
        {
            if (ModelState.IsValid)
            {
                using (UserModel model = new UserModel())
                {
                    model.Register(c);
                }
                TempData["Criada"] = "Conta criada!";
                return RedirectToAction("Login");
            }
            else
            {
                ViewBag.Mensagem = "Erro";
                return View(c);
            }
        }

        [UserFiltro]
        public ActionResult Transactions()
        {
            var id = ((Session["usuario"] as User).IdUsuario);
            using (UserModel model = new UserModel())
            {
                //model.ReadList(id); // atual.
                var lista = model.ReadList(id);
                return View(lista);
            }
            //return View();
        }

        //[HttpPost]
        //public ActionResult Transactions()
        //{
        //    if (ModelState.IsValid)
        //    {
        //        using (UserModel model = new UserModel())
        //        {
        //        }
        //        TempData["Criada"] = "Conta criada!";
        //        return RedirectToAction("Login");
        //    }
        //    else
        //    {
        //        ViewBag.Mensagem = "Erro";
        //        return View();
        //    }
        //}

        [UserFiltro]
        public ActionResult Reports()
        {
            return View();
        }

        [UserFiltro]
        public ActionResult Contacts()
        {
            return View();
        }
    }
}