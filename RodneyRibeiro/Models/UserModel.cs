﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace RodneyRibeiro.Models
{
    public class UserModel : ModelBase
    {
        public void Register(User c)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"Exec CadUsuario 1, @email, @nome, @sobrenome, null, @senha";

            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.Parameters.AddWithValue("@nome", c.Nome);
            cmd.Parameters.AddWithValue("@sobrenome", c.Sobrenome);
            //cmd.Parameters.AddWithValue("@nascimento", null);
            cmd.Parameters.AddWithValue("@senha", c.Senha);

            cmd.ExecuteNonQuery();
        }

        public User Read(string email, string senha)
        {
            User e = null;

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"Exec Logar @email, @senha";
            cmd.Parameters.AddWithValue("@email", value: email);
            cmd.Parameters.AddWithValue("@senha", value: senha);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                e = new User();
                e.IdUsuario = (int)reader["id"];
                e.Status = (int)reader["status"];
                e.Email = (string)reader["email"];
                e.Nome = (string)reader["nome"];
                e.Sobrenome = (string)reader["sobrenome"];
                e.DataNasc = (DateTime)reader["dataNasc"];
                int IdUser = (int)e.IdUsuario;

                SqlCommand cmd2 = new SqlCommand();
                cmd2.Connection = connection;
                cmd2.CommandText = @"Exec VerSaldo @id";
                cmd2.Parameters.AddWithValue("@id", value: IdUser);

                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.Read())
                {
                    e.Saldo = (decimal)reader2["Saldo"];
                    SqlCommand cmd3 = new SqlCommand();
                    cmd3.Connection = connection;
                    cmd3.CommandText = @"Exec VerDesp @id";
                    cmd3.Parameters.AddWithValue("@id", value: IdUser);

                    SqlDataReader reader3 = cmd3.ExecuteReader();

                    if (reader3.Read())
                    {
                        e.Despesa = (decimal)reader3["Despesa"];
                    }
                }
            }
            return e;
        }

        public User ReadList(int id)
        {
            User l = new User();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;

            cmd.CommandText = @"exec ConsultarLL @id_usuario";
            cmd.Parameters.AddWithValue("@id_usuario", id);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                l.NumLinha1 = (int)reader["ID1"];
                l.NumLinha2 = (int)reader["ID2"];
                l.NumLinha3 = (int)reader["ID3"];
                l.NumLinha4 = (int)reader["ID4"];
                l.NumLinha5 = (int)reader["ID5"];
                l.NumLinha6 = (int)reader["ID6"];
                l.NumLinha7 = (int)reader["ID7"];
                l.NumLinha8 = (int)reader["ID8"];
                l.NumLinha9 = (int)reader["ID9"];
                l.NumLinha10 = (int)reader["ID10"];
                l.NumLinha11 = (int)reader["ID11"];
                l.NumLinha12 = (int)reader["ID12"];
                l.NumLinha13 = (int)reader["ID13"];
                l.NumLinha14 = (int)reader["ID14"];
                l.NumLinha15 = (int)reader["ID15"];
                l.Codigo1 = (int)reader["Codigo1"];
                l.Codigo2 = (int)reader["Codigo2"];
                l.Codigo3 = (int)reader["Codigo3"];
                l.Codigo4 = (int)reader["Codigo4"];
                l.Codigo5 = (int)reader["Codigo5"];
                l.Codigo6 = (int)reader["Codigo6"];
                l.Codigo7 = (int)reader["Codigo7"];
                l.Codigo8 = (int)reader["Codigo8"];
                l.Codigo9 = (int)reader["Codigo9"];
                l.Codigo10 = (int)reader["Codigo10"];
                l.Codigo11 = (int)reader["Codigo11"];
                l.Codigo12 = (int)reader["Codigo12"];
                l.Codigo13 = (int)reader["Codigo13"];
                l.Codigo14 = (int)reader["Codigo14"];
                l.Codigo15 = (int)reader["Codigo15"];
                l.Descricao1 = (string)reader["Descricao1"];
                l.Descricao2 = (string)reader["Descricao2"];
                l.Descricao3 = (string)reader["Descricao3"];
                l.Descricao4 = (string)reader["Descricao4"];
                l.Descricao5 = (string)reader["Descricao5"];
                l.Descricao6 = (string)reader["Descricao6"];
                l.Descricao7 = (string)reader["Descricao7"];
                l.Descricao8 = (string)reader["Descricao8"];
                l.Descricao9 = (string)reader["Descricao9"];
                l.Descricao10 = (string)reader["Descricao10"];
                l.Descricao11 = (string)reader["Descricao11"];
                l.Descricao12 = (string)reader["Descricao12"];
                l.Descricao13 = (string)reader["Descricao13"];
                l.Descricao14 = (string)reader["Descricao14"];
                l.Descricao15 = (string)reader["Descricao15"];
                l.Num1 = (string)reader["Num1"];
                l.Num2 = (string)reader["Num2"];
                l.Num3 = (string)reader["Num3"];
                l.Num4 = (string)reader["Num4"];
                l.Num5 = (string)reader["Num5"];
                l.Num6 = (string)reader["Num6"];
                l.Num7 = (string)reader["Num7"];
                l.Num8 = (string)reader["Num8"];
                l.Num9 = (string)reader["Num9"];
                l.Num10 = (string)reader["Num10"];
                l.Num11 = (string)reader["Num11"];
                l.Num12 = (string)reader["Num12"];
                l.Num13 = (string)reader["Num13"];
                l.Num14 = (string)reader["Num14"];
                l.Num15 = (string)reader["Num15"];
                l.Historico1 = (string)reader["Historico1"];
                l.Historico2 = (string)reader["Historico2"];
                l.Historico3 = (string)reader["Historico3"];
                l.Historico4 = (string)reader["Historico4"];
                l.Historico5 = (string)reader["Historico5"];
                l.Historico6 = (string)reader["Historico6"];
                l.Historico7 = (string)reader["Historico7"];
                l.Historico8 = (string)reader["Historico8"];
                l.Historico9 = (string)reader["Historico9"];
                l.Historico10 = (string)reader["Historico10"];
                l.Historico11 = (string)reader["Historico11"];
                l.Historico12 = (string)reader["Historico12"];
                l.Historico13 = (string)reader["Historico13"];
                l.Historico14 = (string)reader["Historico14"];
                l.Historico15 = (string)reader["Historico15"];
                l.Entrada1 = (string)reader["Entrada1"];
                l.Entrada2 = (string)reader["Entrada2"];
                l.Entrada3 = (string)reader["Entrada3"];
                l.Entrada4 = (string)reader["Entrada4"];
                l.Entrada5 = (string)reader["Entrada5"];
                l.Entrada6 = (string)reader["Entrada6"];
                l.Entrada7 = (string)reader["Entrada7"];
                l.Entrada8 = (string)reader["Entrada8"];
                l.Entrada9 = (string)reader["Entrada9"];
                l.Entrada10 = (string)reader["Entrada10"];
                l.Entrada11 = (string)reader["Entrada11"];
                l.Entrada12 = (string)reader["Entrada12"];
                l.Entrada13 = (string)reader["Entrada13"];
                l.Entrada14 = (string)reader["Entrada14"];
                l.Entrada15 = (string)reader["Entrada15"];
                l.Saida1 = (string)reader["Saida1"];
                l.Saida2 = (string)reader["Saida2"];
                l.Saida3 = (string)reader["Saida3"];
                l.Saida4 = (string)reader["Saida4"];
                l.Saida5 = (string)reader["Saida5"];
                l.Saida6 = (string)reader["Saida6"];
                l.Saida7 = (string)reader["Saida7"];
                l.Saida8 = (string)reader["Saida8"];
                l.Saida9 = (string)reader["Saida9"];
                l.Saida10 = (string)reader["Saida10"];
                l.Saida11 = (string)reader["Saida11"];
                l.Saida12 = (string)reader["Saida12"];
                l.Saida13 = (string)reader["Saida13"];
                l.Saida14 = (string)reader["Saida14"];
                l.Saida15 = (string)reader["Saida15"];
                return l;
            }

            return l;
        }
    }
}