0
create database Aquarius
go

use Aquarius
go

create table usuarios
(
	id        int          not null	 primary key identity,
	status	  int,
	email     varchar(250) not null  unique,
	nome      varchar(50)  not null,
	sobrenome varchar(50)  not null,
	dataNasc  date,
	senha 	  varchar(20)  not null,
	check (status in (1,2,3,4))
)
go

--select * from usuarios
--insert into usuarios values (1, 'gust@email.com', 'Gustavo', 'Vilas', '11/06/1996', '123456')
--insert into usuarios values (1, 'rodney@email.com', 'Rodney', 'Ribeiro', '21/02/1973', '123456')

create table listas
(
	id        	int          not null primary key identity,
	mes  		int			not null,
	ano			int			not null,
	id_user		int			 not null references usuarios
)
go

--select * from listas
--insert into listas values (MONTH(GETDATE()), YEAR(GETDATE()), 1)
--insert into listas values (MONTH(GETDATE()), YEAR(GETDATE()), 2)

--create table codigos
--(
--	codigo		int	not null primary key,
--	descricao	varchar(100)
--)
--go

create table linha_lista
(
	id			int not null identity primary key,
	codigo		int,
	descricao	varchar(50),
	num			varchar(5),
	historico	varchar(100),
	entrada		decimal(10,2),
	saida		decimal(10,2),
	id_lista	int not null references listas,
	id_user		int not null references listas
)
go

create table contatos
(
	id			int	not null identity primary key,
	tipo_p		int not null,
	cat_p		int not null,
	nome		varchar(50),
	cpf			int,
	email		varchar(50),
	telefone	varchar(10),
	celular		varchar(10),
	endereco	varchar(50),
	bairro		varchar(50),
	numero		int,
	cep			varchar(8),
	estado		varchar(2),
	aniversario varchar(10),
	sobre		varchar(200),
	id_user		int not null references usuarios,
	check (tipo_p in(1,2)),
	check (cat_p in(1,2,3))
)
go

create procedure CadContato
(
	@tipo_p			int,
	@cat_p			int,
	@nome			varchar(50),
	@cpf			int,
	@email			varchar(50),
	@telefone		varchar(10),
	@celular		varchar(10),
	@endereco		varchar(50),
	@bairro			varchar(50),
	@numero			int,
	@cep			varchar(8),
	@estado			varchar(2),
	@aniversario	varchar(10),
	@sobre			varchar(200),
	@id_user		int
)
as
begin
	insert into contatos values (@tipo_p, @cat_p, @nome, @cpf, @email, @telefone, @celular, @endereco, @bairro, @numero, @cep,
									@estado, @aniversario, @sobre, @id_user)
end
go

--select * from contatos
--exec CadContato 1,3,'Policia', null, 'police@rpd.com', '17 1234-4567', null, 'Av. Raccoon City', 'Alameda Zombie', '696', '15041-654', 'TX', null, 'Teste', 1

--select * from linha_lista
--insert into linha_lista values (1001, 'Batata', 'Chq27', 'Um cheque de batata', 2400.80, 0.00,1,1)
--insert into linha_lista values (1002, 'Cebola', '27', 'Um saida de cebola', 0.00, 2100.00,1,1)
--insert into linha_lista values (1002, 'Cebola', '27', 'Um saida de cebola', 0.00, 2100.00,2,2)
--insert into linha_lista values (1002, 'Cebola', '27', 'Um saida de cebola', 0.00, 2100.00,3,1)


create procedure CadUsuario
(
	@status	     int,
	@email       varchar(250),
	@nome        varchar(50),
	@sobreNome   varchar(50),
	@dataNasc    date,
	@senha 	     varchar(16)
)
as
begin
	insert into usuarios values (@status, @email, @nome, @sobreNome, '1950-01-01', @senha)
	insert into listas values (MONTH(GETDATE()), YEAR(GETDATE()), @@IDENTITY)
end
go

--drop table usuarios
--drop table listas
--drop table linha_lista

--select * from usuarios
--select * from listas
--select * from linha_lista
--exec CadUsuario 1, 'gust@email.com', 'Gust', 'Vilas', '1950-01-01', '123456'
--exec CadUsuario 1, 'rodney@email.com', 'Rodney', 'Ribeiro', '1940-01-01', '123456'
--exec CadUsuario 1, 'test@email.com', 'Teste', 'Novo', '1940-01-01', '123456'
--Insert into listas values ('11', '2017', '1002')



create procedure Logar
(
	@email varchar(250),
	@senha varchar(16)
)
as
begin
	select * from usuarios where email = @email and senha = @senha
end
go

create procedure VerSaldo
(
	@id int
)
as
begin
	select SUM(entrada) Saldo from linha_lista where id_user = @id;
end
go

--exec VerSaldo 1
--exec VerDesp 1
--Update linha_lista set entrada='1.10' where codigo = '0'
--Update linha_lista set saida='2.20' where codigo = '0'

create procedure VerDesp
(
	@id int
)
as
begin
	select SUM(saida) Despesa from linha_lista where id_user = @id;
end
go

create Procedure EditLista
(
	@iduser			int,
	@lista_id		int,
	@numlinha		int,
	@codigo			int,
	@descricao		varchar(100),
	@num			varchar(5),
	@historico		varchar(100),
	@entrada		varchar(10),
	@saida			varchar(10)
)
as
begin
	--declare @descricao varchar(100)
	--declare @id_user int
	--set @descricao = (select descricao from codigos where codigo = @codigo)
	--set @lista = (select id from listas where id_user = @iduser)
	--select * from v_linha_lista
	UPDATE linha_lista set 
		codigo = @codigo, descricao = @descricao, num = @num, historico = @historico, entrada = @entrada, saida = @saida where id = @numlinha and id_lista = @lista_id and id_user = @iduser
end
Go

create trigger trg_createlist
on Listas
after insert
as
begin
	declare @contador int;
	declare @id_user int, @id_lista int;
	set @contador = 0;

	select @id_user = 1, @id_lista = 1 from inserted;

	while @contador <= 14
	begin
		insert into linha_lista values (0, '', '', '', '1.10' , '2.20', 1, 1);
		set @contador = @contador + 1;
	end
end
go

--select * from linha_lista
--Select * from v_linha_lista
--Select * from v_linha_lista where IDUser = 1
--exec EditLista 1, 1, 1, 1001,'Batata','Chq7', 'Remember me for centuries', '2010.10', '0.00'
--exec EditLista 1002, 1004, 1, 1002, "Xamans" ,'27', 'We could be imortals', '9010.20', '0.00'
--exec EditLista 1, 1, 3, 1003, 'Chq4', 'Sono chi no sadame', '5230.80', '0.00'

create view v_usuario
as
select u.id			ID,
	   u.status		Status,
	   u.email		Email,
	   u.nome		Nome,
	   u.sobrenome	Sobrenome,
	   u.dataNasc	Nascimento,
	   u.senha		Senha
from usuarios u
go

create view v_listas
as
select l.id			ID,
	   l.mes		Mes,
	   l.ano		Ano,
	   l.id_user	ID_User
from listas l
go

create view v_linha_lista
as
select ll.id		ID,
	   ll.codigo	Codigo,
	   ll.descricao	Descricao,
	   ll.num		Num,
	   ll.historico	Historico,
	   ll.entrada	Entrada,
	   ll.saida		Saida,
	   ll.id_lista	IDLista,
	   ll.id_user	IDUser
from linha_lista ll
go

--exec ConsultarLL 1
create Procedure ConsultarLL
(
	@IDUser		int
)
as
begin
	declare 	@ID1			int;
	declare 	@ID2			int;
	declare 	@ID3			int;
	declare 	@ID4			int;
	declare 	@ID5			int;
	declare 	@ID6			int;
	declare 	@ID7			int;
	declare 	@ID8			int;
	declare 	@ID9			int;
	declare 	@ID10			int;
	declare 	@ID11			int;
	declare 	@ID12			int;
	declare 	@ID13			int;
	declare 	@ID14			int;
	declare 	@ID15			int;
	declare 	@Codigo1		int;
	declare 	@Codigo2		int;
	declare 	@Codigo3		int;
	declare 	@Codigo4		int;
	declare 	@Codigo5		int;
	declare 	@Codigo6		int;
	declare 	@Codigo7		int;
	declare 	@Codigo8		int;
	declare 	@Codigo9		int;
	declare 	@Codigo10		int;
	declare 	@Codigo11		int;
	declare 	@Codigo12		int;
	declare 	@Codigo13		int;
	declare 	@Codigo14		int;
	declare 	@Codigo15		int;
	declare 	@Descricao1		varchar(100);
	declare 	@Descricao2		varchar(100);
	declare 	@Descricao3		varchar(100);
	declare 	@Descricao4		varchar(100);
	declare 	@Descricao5		varchar(100);
	declare 	@Descricao6		varchar(100);
	declare 	@Descricao7		varchar(100);
	declare 	@Descricao8		varchar(100);
	declare 	@Descricao9		varchar(100);
	declare 	@Descricao10	varchar(100);
	declare 	@Descricao11	varchar(100);
	declare 	@Descricao12	varchar(100);
	declare 	@Descricao13	varchar(100);
	declare 	@Descricao14	varchar(100);
	declare 	@Descricao15	varchar(100);
	declare 	@Num1			varchar(100);
	declare 	@Num2			varchar(100);
	declare 	@Num3			varchar(100);
	declare 	@Num4			varchar(100);
	declare 	@Num5			varchar(100);
	declare 	@Num6			varchar(100);
	declare 	@Num7			varchar(100);
	declare 	@Num8			varchar(100);
	declare 	@Num9			varchar(100);
	declare 	@Num10			varchar(100);
	declare 	@Num11			varchar(100);
	declare 	@Num12			varchar(100);
	declare 	@Num13			varchar(100);
	declare 	@Num14			varchar(100);
	declare 	@Num15			varchar(100);
	declare 	@Historico1		varchar(100);
	declare 	@Historico2		varchar(100);
	declare 	@Historico3		varchar(100);
	declare 	@Historico4		varchar(100);
	declare 	@Historico5		varchar(100);
	declare 	@Historico6		varchar(100);
	declare 	@Historico7		varchar(100);
	declare 	@Historico8		varchar(100);
	declare 	@Historico9		varchar(100);
	declare 	@Historico10	varchar(100);
	declare 	@Historico11	varchar(100);
	declare 	@Historico12	varchar(100);
	declare 	@Historico13	varchar(100);
	declare 	@Historico14	varchar(100);
	declare 	@Historico15	varchar(100);
	declare 	@Entrada1		varchar(100);
	declare 	@Entrada2		varchar(100);
	declare 	@Entrada3		varchar(100);
	declare 	@Entrada4		varchar(100);
	declare 	@Entrada5		varchar(100);
	declare 	@Entrada6		varchar(100);
	declare 	@Entrada7		varchar(100);
	declare 	@Entrada8		varchar(100);
	declare 	@Entrada9		varchar(100);
	declare 	@Entrada10		varchar(100);
	declare 	@Entrada11		varchar(100);
	declare 	@Entrada12		varchar(100);
	declare 	@Entrada13		varchar(100);
	declare 	@Entrada14		varchar(100);
	declare 	@Entrada15		varchar(100);
	declare 	@Saida1			varchar(100);
	declare 	@Saida2			varchar(100);
	declare 	@Saida3			varchar(100);
	declare 	@Saida4			varchar(100);
	declare 	@Saida5			varchar(100);
	declare 	@Saida6			varchar(100);
	declare 	@Saida7			varchar(100);
	declare 	@Saida8			varchar(100);
	declare 	@Saida9			varchar(100);
	declare 	@Saida10		varchar(100);
	declare 	@Saida11		varchar(100);
	declare 	@Saida12		varchar(100);
	declare 	@Saida13		varchar(100);
	declare 	@Saida14		varchar(100);
	declare 	@Saida15		varchar(100);
	declare 	@MarcadorID		int;
	declare		@Contador		int;

	set @MarcadorID = (select MIN(ID) from v_linha_lista where IDUser = @IDUser);
	set @Contador = 1;
	
	while @Contador <= 15
	begin
		if @Contador = 1
		begin
			set @ID1 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo1 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao1 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num1 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico1 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada1 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida1 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 2
		begin
			set @ID2 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo2 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao2 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num2 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico2 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada2 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida2 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 3
		begin
			set @ID3 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo3 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao3 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num3 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico3 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada3 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida3 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 4
		begin
			set @ID4 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo4 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao4 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num4 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico4 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada4 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida4 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 5
		begin
			set @ID5 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo5 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao5 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num5 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico5 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada5 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida5 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 6
		begin
			set @ID6 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo6 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao6 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num6 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico6 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada6 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida6 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 7
		begin
			set @ID7 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo7 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao7 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num7 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico7 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada7 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida7 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 8
		begin
			set @ID8 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo8 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao8 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num8 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico8 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada8 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida8 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 9
		begin
			set @ID9 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo9 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao9 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num9 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico9 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada9 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida9 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 10
		begin
			set @ID10 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo10 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao10 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num10 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico10 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada10 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida10 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 11
		begin
			set @ID11 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo11 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao11 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num11 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico11 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada11 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida11 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 12
		begin
			set @ID12 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo12 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao12 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num12 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico12 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada12 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida12 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 13
		begin
			set @ID13 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo13 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao13 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num13 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico13 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada13 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida13 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 14
		begin
			set @ID14 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo14 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao14 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num14 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico14 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada14 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida14 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		if @Contador = 15
		begin
			set @ID15 = (select ID from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Codigo15 = (select Codigo from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Descricao15 = (select Descricao from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Num15 = (select Num from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Historico15 = (select Historico from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Entrada15 = (select Entrada from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
			set @Saida15 = (select Saida from v_linha_lista where IDUser = @IDUser and ID = @MarcadorID);
		end

		set @contador = @contador +1;
		set @MarcadorID = @MarcadorID +1;
	end
end
Go

--exec ConsultarLL 1

-- C�digos que precisam ser autom�ticamente adicionados no BD --
insert into codigos values (1001, 'Vendas/Revendedores')
insert into codigos values (1002, 'Vendas/Consumidor Final')
insert into codigos values (1003, 'Vendas/Reservas')
insert into codigos values (1004, 'Empr�stimo Banco do Brasil')
insert into codigos values (1005, 'Empr�stimo Banco Nossa Caixa')
insert into codigos values (1006, 'Empr�stimo Banco Bradesco')
insert into codigos values (1007, 'Aplica��o Financeira')
insert into codigos values (1008, 'Vendas/Diversas')
--
insert into codigos values (2000, '�gua Escrit�rio/F�brica')
insert into codigos values (2001, 'Alugu�is')
insert into codigos values (2002, 'Assessoria')
insert into codigos values (2003, 'Nextel')
insert into codigos values (2004, 'Combust�vel')
insert into codigos values (2005, 'Contador')
insert into codigos values (2006, 'Folha/Vale')
insert into codigos values (2007, 'Fretes')
insert into codigos values (2008, 'Indeniza��es/13�/F�rias')
insert into codigos values (2009, 'INSS/FGTS')
insert into codigos values (2010, 'Internet/Hospedagem')
insert into codigos values (2011, 'Investimento F�brica')
insert into codigos values (2012, 'IPTU')
insert into codigos values (2013, 'Luz')
insert into codigos values (2014, 'Manuten��o Computador')
insert into codigos values (2015, 'Material de Seguran�a e Uniformes')
insert into codigos values (2016, 'Papelaria, C�pias e Correio')
insert into codigos values (2017, 'Seguran�a')
insert into codigos values (2018, 'Compra de Maquin�rio')
insert into codigos values (2019, 'Sindicatos')
insert into codigos values (2020, 'Telefones')
insert into codigos values (2021, 'Viagens')
insert into codigos values (2022, 'Medicina/Seguran�a do Trabalho')
insert into codigos values (2023, 'Ferramentas')
insert into codigos values (2024, 'Investimento Escrit�rio')
insert into codigos values (2025, 'Vale Transporte')
insert into codigos values (2026, 'Marketing')
insert into codigos values (2027, 'Inadinpl�ncia')
insert into codigos values (2028, 'Lanches e Refei��es')
--
insert into codigos values (3000, 'Acr�licos')
insert into codigos values (3001, 'MDF, PVC, P.S')
insert into codigos values (3002, 'Colas e Resinas')
insert into codigos values (3003, 'Embalagem')
insert into codigos values (3004, 'Materiais de consumo produ��o')
insert into codigos values (3005, 'Tintas, Verniz')
insert into codigos values (3006, 'Manuten��o de M�quinas')
insert into codigos values (3007, 'Manuten��o de Ve�culos')
insert into codigos values (3008, 'Manuten��o Pr�dio')
insert into codigos values (3009, 'Material de Seguran�a e Uniformes')
insert into codigos values (3010, 'Compra de M�quinario')
insert into codigos values (3011, 'Ferramentas')
--	